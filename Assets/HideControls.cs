﻿using UnityEngine;
using System.Collections;

public class HideControls : MonoBehaviour
{
    public GameObject jump, up, down, left, right, playBtn, timer;

    // Use this for initialization
    void Start()
    {
        jump = GameObject.FindGameObjectWithTag("btn_jump");
        up = GameObject.FindGameObjectWithTag("btn_up");
        down = GameObject.FindGameObjectWithTag("btn_down");
        left = GameObject.FindGameObjectWithTag("btn_left");
        right = GameObject.FindGameObjectWithTag("btn_right");
        playBtn = GameObject.FindGameObjectWithTag("btn_play");
        timer = GameObject.FindGameObjectWithTag("time_BG");
        GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = false;
        jump.SetActive(false);

        up.SetActive(false);
        down.SetActive(false);
        left.SetActive(false);
        right.SetActive(false);
        timer.SetActive(false);
        Time.timeScale = 0;



    }

    public void playBtn_Click()
    {
        Debug.Log("Play Button");



        Time.timeScale = 1;


        jump.SetActive(true);
        up.SetActive(true);
        down.SetActive(true);
        left.SetActive(true);
        right.SetActive(true);
        timer.SetActive(true);

        playBtn.SetActive(false);


    }
    // Update is called once per frame
    void Update()
    {

    }

}