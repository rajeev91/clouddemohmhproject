﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Timer : MonoBehaviour {
    public Text timertext;
    private float startTime;
	// Use this for initialization
	void Start () {
        startTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        float t = Time.time - startTime;
        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("f0");
        //timertext.text = "0" + minutes + ":" + seconds; //(seconds < 10 ? "0" + seconds : seconds);
        timertext.text = "0" + minutes + ":" + (Convert.ToInt32(seconds) < 10 ? "0" + seconds : seconds);  
    }
}
